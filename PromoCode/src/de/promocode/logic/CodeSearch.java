package de.promocode.logic;

public class CodeSearch {

	public static int NOT_FOUND = -1;

	/**
	 * Sucht einen Code in der Liste.
	 * 
	 * @param list
	 *            Liste der Codes im Long-Format
	 * @param searchValue
	 *            Gesuchter Code
	 * @return Index des Codes, wenn er vorhanden ist, NOT_FOUND wenn er nicht
	 *         gefunden werden konnte
	 */
	public static int findPromoCode(long[] list, long searchValue) {
		int von = 0;
		int bis = list.length;
		int pos = NOT_FOUND;
		
		
			do {
				if( list[von]> searchValue ||  list[bis-1] < searchValue) 
					return NOT_FOUND;
				else {
				int m = (von+bis)/2;
				
				if(list[m] == searchValue)
					pos = m;
				else if(list[m]>searchValue)
					bis = m-1;
				else
					von = m+1;
				}
			} while(pos == -1);
			return pos;
			
	}
}