import java.lang.reflect.Array;

public class Mergesort {
private	int zaehler = 0;



	public void mergesort(int[] liste, int l, int r) {
	if(l< r) {
		int m = (l+r)/ 2;
		
		mergesort(liste,l,m);
		mergesort(liste,m+1,r);
		
		merge(liste,l,m,r);
	}
		
		
	}	
		
	public void merge(int[] liste,int l, int m, int r) {
		int n1 = m-l +1;
		int n2 = r-m;
		
		int left[] = new int[n1];
		int right[] = new int[n2];
		
		for(int i =0;i<n1;i++) {
			left[i] = liste[l+i];
		}
		for(int j =0;j<n2;j++) {
			right[j] = liste[m+1+j];
		}
		int i = 0;
		int j = 0;
		
		int k = l;
		
		while(i < n1 && j < n2) {
			if(left[i] <= right[j]) {
				liste[k] = left[i];
				i++;
				System.out.println(zaehler + " " + "<->" + " " + (zaehler +1) + "   " + "{ " + this.toString(liste)+ " }");
				zaehler++;
			} else {
				liste[k] = right[j];
				j++;
				System.out.println(zaehler + " " + "<->" + " " + (zaehler +1) + "   " + "{ " + this.toString(liste)+ " }");
				zaehler++;
			}
			k++;
		}
		while (i<n1) {
			liste[k] = left[i];
			i++;
			k++;
			System.out.println(zaehler + " " + "<->" + " " + (zaehler +1) + "   " + "{ " + this.toString(liste)+ " }");
			zaehler++;
		}
		while(j<n2) {
			liste[k] = right[j];
			j++;
			k++;
			System.out.println(zaehler + " " + "<->" + " " + (zaehler +1) + "   " + "{ " + this.toString(liste)+ " }");
			zaehler++;
		}
}
	
	
	
	public String toString(int[] liste) {
		String output = "";
		for (int i = 0; i < liste.length; i++) {
			output += liste[i];
			output += ", ";
		}

		return output;
	}

	public int getZaehler() {
		return zaehler;
	}

	public void setZaehler(int zaehler) {
		this.zaehler = zaehler;
	}

	
}