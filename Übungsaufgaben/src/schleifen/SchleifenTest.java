package schleifen;

import java.util.Scanner;

public class SchleifenTest {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int anzahl;
		System.out.print("Bitte geben sie eine Zahl ein: ");
		anzahl = input.nextInt();
		for (int i = 0; i < anzahl; i++) {
			System.out.print("+");
		}

		int i = 0;
		while (i < anzahl) {
			System.out.print("-");
			i++;
		}
		i = 0;
		do {
			System.out.print("*");
			i++;
		} while (i < anzahl);

	}

}
