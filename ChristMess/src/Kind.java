import java.util.*;

public class Kind implements Comparable<Kind>{
	@Override
	public String toString() {
		return "Kind [vorname=" + vorname + ", nachname=" + nachname + ", geburtstag=" + geburtstag + ", wohnort="
				+ wohnort + ", bravheitsgrad=" + bravheitsgrad + "]";
	}
	//Jedes Kind hat einen Nachnamen, Vornamen, Geburtstag, Wohnort und Bravheitsgrad
	//Erstellen Sie einen vollparametrisierten Konstruktor, Getter/Setter und eine toString-Methode
	private String vorname,nachname,geburtstag,wohnort;
	private int bravheitsgrad;
	public Kind(String vorname, String nachname, String geburtstag, int bravheitsgrad, String wohnort) {
		super();
		this.vorname = vorname;
		this.nachname = nachname;
		this.geburtstag = geburtstag;
		this.wohnort = wohnort;
		this.bravheitsgrad = bravheitsgrad;
	}
	public String getVorname() {
		return vorname;
	}
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	public String getNachname() {
		return nachname;
	}
	public void setNachname(String nachname) {
		this.nachname = nachname;
	}
	public String getGeburtstag() {
		return geburtstag;
	}
	public void setGeburtstag(String geburtstag) {
		this.geburtstag = geburtstag;
	}
	public String getWohnort() {
		return wohnort;
	}
	public void setWohnort(String wohnort) {
		this.wohnort = wohnort;
	}
	public int getBravheitsgrad() {
		return bravheitsgrad;
	}
	public void setBravheitsgrad(int bravheitsgrad) {
		this.bravheitsgrad = bravheitsgrad;
	}
	@Override
	public int compareTo(Kind o) {
		int i = Integer.compare(this.bravheitsgrad, o.bravheitsgrad);
		if(i != 0)
			return i;
		return  this.wohnort.compareTo(o.getWohnort());	
	}
}
