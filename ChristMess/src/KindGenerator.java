import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class KindGenerator {
File f = new File("kinddaten.txt");


public List<Kind> generateKinderListe(){

	List<Kind> liste = new ArrayList<Kind>();
	try {
		FileReader fr = new FileReader(f);
		BufferedReader br = new BufferedReader(fr);
		String s;
		while((s = br.readLine()) != null) {
			s = s.replaceAll(",", "");
			String[] temp = s.split(" ");
			
			int bravheitsgrad = Integer.parseInt(temp[3]);
			String wohnort = "";
			for(int i = 4; i<temp.length;i++)
			 wohnort = wohnort + temp[i] + " ";
		
			Kind kind = new Kind(temp[0],temp[1],temp[2],bravheitsgrad,wohnort);
			liste.add(kind);
		}
	} catch (Exception e) {
		System.out.println("Yeet");
		e.printStackTrace();
	}	
	return liste;
}

}