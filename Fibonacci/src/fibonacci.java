import java.util.ArrayList;

public class fibonacci {
ArrayList<Integer> liste = new ArrayList<Integer>();
	public ArrayList<Integer> fibonacciIterativ(int m) {
		liste.add(0,1);
		liste.add(1,1);
		
		for (int i = 2; i < m; i++) {
			 int e = liste.get(i-1) + liste.get(i-2);
			 liste.add(e);
			
		}
		return liste;
	}
	public ArrayList<Integer> fibonacciRekursiv(int m, int i) {
		while(i<m) {
		if(i < 2)
		liste.add(1);
		else
		liste.add(liste.get(i-1)+liste.get(i-2));	
		return fibonacciRekursiv(m,i+1);
		}
		return liste;
	}
}
