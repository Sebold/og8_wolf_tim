import java.util.Scanner;
public class PrimzahlTest {

	public static void main(String[] args) {
	Scanner scanny = new Scanner(System.in);	
	Primzahl p = new Primzahl();
	Stoppuhr t = new Stoppuhr();
	boolean exit = true;
	
	while(exit == true) {
	System.out.println("Bitte die zu testende Zahl eingeben: ");	
	long primzahl = scanny.nextLong();
	
	t.start();
	boolean result = p.istPrimzahl(primzahl);
	t.end();
	
	System.out.println(result + " in " + t.getResult() + " Millisekunden");
	System.out.println("(w)iederholen");
	System.out.println("(b)eenden");
	if(scanny.next().charAt(0) != 'w')
		exit = false;
		}
	}

}
