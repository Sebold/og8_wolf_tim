
public class KeyStore01 {
	private String[] KeyStore;

	public KeyStore01() {
		this.KeyStore = new String[100];
	}

	public KeyStore01(int length) {
		this.KeyStore = new String[length];
	}

	public boolean add(String input) {
		for (int i = 0; i < KeyStore.length; i++) {

			if (this.KeyStore[i] == null) {
				this.KeyStore[i] = input;

				return true;
			}
		}
		return false;
	}

	public int indexOf(String input) {

		for (int i = 0; i < this.KeyStore.length; i++) {

			if (input.equals(this.KeyStore[i])) {
				return i;
			}

		}
		return -1;
	}

	public void remove(int input) {
		this.KeyStore[input] = null;

	}

	public void remove(String input) {
		for (int i = 0; i < this.KeyStore.length; i++) {
			if (input.equals(this.KeyStore[i])) {
				this.KeyStore[i] = null;
			}
		}
	}

	@Override
	public String toString() {
		String liste = "";
		for (int i = 0; i < this.KeyStore.length; i++) {
			if (this.KeyStore[i] != null) {
				liste += this.KeyStore[i] + ", ";
			}

		}
		return liste;
	}

	public void clear() {
		for (int i = 0; i < this.KeyStore.length; i++) {
			this.KeyStore[i] = null;
		}
	}

	public String get(int input) {
		return this.KeyStore[input];
	}

	public int size() {
		int liste = 0;
		for (int i = 0; i < this.KeyStore.length; i++) {
			if (this.KeyStore[i] != null) {
				liste ++;
			}
		}
		return liste;
	}
}
