package de.futurehome.tanksimulator;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyActionListener implements ActionListener {
	public TankSimulator f;
	public Log log;
	
	private String einfuellen = "Der Tank wurde gef�llt";
	private String verbrauchen = "Der Tank wurde etwas verbraucht";
	private String zuruecksetzen = "Der Tank wurde zur�ckgesetzt";
	
	public MyActionListener(TankSimulator f) {
		this.f = f;
		
	}

	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == f.btnBeenden)
			System.exit(0);
		
		if (obj == f.btnEinfuellen) {
			 double fuellstand = f.myTank.getFuellstand();
			 if(fuellstand + 5 <= 100)
			 fuellstand = fuellstand + 5;
			 else {
				fuellstand = 100; 
			 }
			 f.myTank.setFuellstand(fuellstand);
			 f.progressBar.setValue((int) fuellstand);
			 f.lblFuellstand.setText("");
			 
			 log.setText(einfuellen);
			
		}
		if(obj == f.btnVerbrauchen) {
			double fuellstand = f.myTank.getFuellstand();
			if((fuellstand - f.slider.getValue() )< 0) {
			f.myTank.setFuellstand(0.0);	
			f.progressBar.setValue((int) fuellstand);
			}
			else {
			fuellstand = fuellstand - f.slider.getValue();
			f.myTank.setFuellstand(fuellstand);
			f.progressBar.setValue((int) fuellstand);
			}
			f.lblFuellstand.setText("");
		}
		if(obj == f.btnZur�cksetzen) {
			f.myTank.setFuellstand(0);
			f.progressBar.setValue(0);
			f.lblFuellstand.setText("");
		}
		
	}
}