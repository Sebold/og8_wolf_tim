
public class Addon {
private double verkaufspreis;
private int eindeutigeID;
private int bestand;
private String bezeichnung;
private int maxBestand;

//-----------------------------------------------------------//

public Addon(double verkaufspreis, int eindeutigeID, int bestand, String bezeichnung, int maxBestand) {
this.verkaufspreis = verkaufspreis;
this.eindeutigeID = eindeutigeID;
this.bestand = bestand;
this.bezeichnung = bezeichnung;
this.maxBestand = maxBestand;
}

public double getVerkaufspreis() {	
	return verkaufspreis;
}
public void setVerkaufspreis(double verkaufspreis) {
	this.verkaufspreis = verkaufspreis;
}
public int getEindeutigeID() {
	return eindeutigeID;
}
public void setEindeutigeID(int eindeutigeID) {
	this.eindeutigeID = eindeutigeID;
}
public int getBestand() {
	return bestand;
}
public void setBestand(int bestand) {
	this.bestand = bestand;
}
public String getBezeichnung() {
	return bezeichnung;
}
public void setBezeichnung(String bezeichnung) {
	this.bezeichnung = bezeichnung;
}
public int getMaxBestand() {
	return maxBestand;
}
public void setMaxBestand(int maxBestand) {
	this.maxBestand = maxBestand;
}

public void kaufen() {
	if(this.bestand >= this.maxBestand) {
		System.out.println("Sie haben bereits den Maximalen Bestand erreicht. Ihr Geld wurde trotzdem genommen :^)");
	}
	else
	this.bestand = this.bestand + 1;	
}

public void verbrauchen() {
	if(this.bestand <= 0) {
		System.out.println("Sie haben kein Item mehr �brig, bitte kaufen sie neue");
	}
	else
	this.bestand = this.bestand - 1;
}

@Override
public String toString() {
	return "Addon [verkaufspreis=" + verkaufspreis + ", eindeutigeID=" + eindeutigeID + ", bestand=" + bestand
			+ ", bezeichnung=" + bezeichnung + ", maxBestand=" + maxBestand + "]";
}
}
